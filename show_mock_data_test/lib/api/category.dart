import 'dart:convert';
import 'package:http/http.dart' as http;

class Categorie {
  final String slug;
  final String name;
  final String url;

  const Categorie({
    required this.slug,
    required this.name,
    required this.url,
  });

  factory Categorie.fromJson(Map<String, dynamic> json) {
    return Categorie(
      slug: json['slug'] ?? "",
      name: json['name'] ?? "",
      url: json['url'] ?? "",
    );
  }
}

Future<List<Categorie>> fetchCategories() async {
  final response = await http
      .get(
        Uri.parse("https://dummyjson.com/products/categories"),
      )
      .timeout(const Duration(seconds: 10));
  if (response.statusCode == 200) {
    List<Categorie> tmpCategories = [];

    for (var categorie in jsonDecode(response.body)) {
      tmpCategories.add(Categorie.fromJson(categorie));
    }

    return tmpCategories;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load categories');
  }
}
