import 'dart:convert';
import 'package:http/http.dart' as http;

class Product {
  final int id;
  final String title;
  final String description;
  final String category;
  final double price;
  final double discountPercentage;
  final double rating;
  final int stock;
  final List<String> tags;
  final String brand;
  final String sku;
  final int weight;
  final Dimension dimensions;
  final String warrantyInformation;
  final String shippingInformation;
  final String availabilityStatus;
  final List<Review> reviews;
  final String returnPolicy;
  final int minimumOrderQuantity;
  final Meta meta;
  final List<String> images;
  final String thumbnail;
  const Product({
    required this.id,
    required this.title,
    required this.description,
    required this.category,
    required this.price,
    required this.discountPercentage,
    required this.rating,
    required this.stock,
    required this.tags,
    required this.brand,
    required this.sku,
    required this.weight,
    required this.dimensions,
    required this.warrantyInformation,
    required this.shippingInformation,
    required this.availabilityStatus,
    required this.reviews,
    required this.returnPolicy,
    required this.minimumOrderQuantity,
    required this.meta,
    required this.images,
    required this.thumbnail,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    List<String> tmpTags = [];
    for (var tag in json['tags']) {
      tmpTags.add(tag);
    }

    List<Review> tmpReviews = [];
    for (var review in json['reviews']) {
      tmpReviews.add(Review.fromJson(review));
    }

    List<String> tmpImages = [];
    for (var tag in json['images']) {
      tmpImages.add(tag);
    }
    return Product(
      id: json['id'] ?? -1,
      title: json['title'] ?? "",
      description: json['description'] ?? "",
      category: json['category'] ?? "",
      price: double.parse((json['price'] ?? 0.0).toString()),
      discountPercentage:
          double.parse((json['discountPercentage'] ?? 0.0).toString()),
      rating: double.parse((json['rating'] ?? 0.0).toString()),
      stock: json['stock'] ?? 0,
      tags: tmpTags,
      brand: json['brand'] ?? "",
      sku: json['sku'] ?? "",
      weight: json['weight'] ?? 0,
      dimensions: Dimension.fromJson(json['dimensions'] ?? {}),
      warrantyInformation: json['warrantyInformation'] ?? "",
      shippingInformation: json['shippingInformation'] ?? "",
      availabilityStatus: json['availabilityStatus'] ?? "",
      reviews: tmpReviews,
      returnPolicy: json['returnPolicy'] ?? "",
      minimumOrderQuantity: json['minimumOrderQuantity'] ?? 0,
      meta: Meta.fromJson(json['meta'] ?? {}),
      images: tmpImages,
      thumbnail: json['thumbnail'] ?? "",
    );
  }
}

class Dimension {
  final double width;
  final double height;
  final double depth;

  const Dimension({
    required this.width,
    required this.height,
    required this.depth,
  });

  factory Dimension.fromJson(Map<String, dynamic> json) {
    return Dimension(
      width: double.parse((json['width'] ?? 0.0).toString()),
      height: double.parse((json['height'] ?? 0.0).toString()),
      depth: double.parse((json['depth'] ?? 0.0).toString()),
    );
  }
}

class Review {
  final int rating;
  final String comment;
  final String date;
  final String reviewerName;
  final String reviewerEmail;
  const Review({
    required this.rating,
    required this.comment,
    required this.date,
    required this.reviewerName,
    required this.reviewerEmail,
  });

  factory Review.fromJson(Map<String, dynamic> json) {
    return Review(
      rating: json['rating'] ?? 0,
      comment: json['comment'] ?? "",
      date: json['date'] ?? "",
      reviewerName: json['reviewerName'] ?? "",
      reviewerEmail: json['reviewerEmail'] ?? "",
    );
  }
}

class Meta {
  final String createdAt;
  final String updatedAt;
  final String barcode;
  final String qrCode;
  const Meta({
    required this.createdAt,
    required this.updatedAt,
    required this.barcode,
    required this.qrCode,
  });

  factory Meta.fromJson(Map<String, dynamic> json) {
    return Meta(
      createdAt: json['createdAt'] ?? "",
      updatedAt: json['updatedAt'] ?? "",
      barcode: json['barcode'] ?? "",
      qrCode: json['qrCode'] ?? "",
    );
  }
}

Future<List<Product>> fetchProducts(String product) async {
  final response = await http
      .get(
        Uri.parse("https://dummyjson.com/products/category/$product"),
      )
      .timeout(const Duration(seconds: 10));

  if (response.statusCode == 200) {
    List<Product> tmpProducts = [];
    dynamic json = jsonDecode(response.body);
    for (var product in json['products']) {
      tmpProducts.add(Product.fromJson(product));
    }

    return tmpProducts;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load categories');
  }
}
