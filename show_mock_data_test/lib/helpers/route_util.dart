import 'package:get/get.dart';
import 'package:show_mock_data_test/constants/constants.dart';
import 'package:show_mock_data_test/screens/home/home_page.dart';
import 'package:show_mock_data_test/screens/product/product_page.dart';

List<GetPage<dynamic>> allRoutes = [
  GetPage<dynamic>(name: Routes.home, page: () => const HomeScreen()),
  GetPage<dynamic>(name: Routes.product, page: () => const ProductScreen()),
];
