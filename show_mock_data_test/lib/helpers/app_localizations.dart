import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:show_mock_data_test/constants/constants.dart';

class AppLocalizations {
  Locale locale;

  // Constructor
  AppLocalizations(this.locale);

  // Helper method to keep the code in widgets concise
  // Localizations are accessed using an InheritedWidget "of syntax"
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations)!;
  }

  // Localized strings map
  late Map<String, String> _localizedStrings;

  // Load the json language file from the "lang folder"
  Future<bool> load() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String lang = prefs.getString('language') ?? 'en';
    final String jsonLang =
        await rootBundle.loadString('assets/lang/$lang.json');
    // Decode string result
    final Map<String, dynamic> langMap = json.decode(jsonLang);
    _localizedStrings =
        langMap.map((key, value) => MapEntry(key, value.toString()));

    return true;
  }

  Future<bool> setlang(lang) async {
    final String jsonLang =
        await rootBundle.loadString('assets/lang/$lang.json');
    // Decode string result
    final Map<String, dynamic> langMap = json.decode(jsonLang);
    _localizedStrings =
        langMap.map((key, value) => MapEntry(key, value.toString()));

    return true;
  }

  // Translate method - will be called from every widget which needs a localized text
  String translate(String key) {
    return _localizedStrings[key] ?? '';
  }

  // Static member to have a simple access to the delegate from the MaterialApp
  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();
}

// LocalizationsDelegate is a factory for a set of localized resources
// In this case, the localized strings will be gotten in an AppLocalizations

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  // This delegate will never change (it doesn't even have fields)
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return SUPPORTED_LOCALES.contains(locale);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    // Init AppLocalizations class where the json loading actually runs
    final AppLocalizations localizations = AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => false;
}
