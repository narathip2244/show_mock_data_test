part of '../product_page.dart';

class ProductScreen extends StatefulWidget {
  const ProductScreen({super.key});

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  ScrollController? _scrollController;
  ThemeData? theme;
  AppLocalizations? i18n;
  List<Product> products = [];
  Map<String, bool> networkError = {};

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    i18n = AppLocalizations.of(context);
    theme = Theme.of(context);
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();

    fetchProducts(Get.arguments["slug"]).then(
      (value) {
        setState(() {
          products = value;
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController!.dispose();
  }

  List<Widget> initResource() {
    List<Widget> tmpProductList = [];
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double imageSize =
        (screenHeight > screenWidth ? screenHeight * 0.2 : screenWidth * 0.2)
            .clamp(80, 120);
    AssetImage backgroundImageFallback =
        const AssetImage('assets/images/empty_pic.jpg');

    for (var product in products) {
      NetworkImage backgroundImage = NetworkImage(product.thumbnail);

      tmpProductList.add(Container(
        decoration: BoxDecoration(
          color: theme!.cardColor,
          borderRadius: BorderRadius.circular(baseBorderRadius),
        ),
        margin: const EdgeInsets.only(bottom: basePadding * 2),
        padding: const EdgeInsets.all(basePadding),
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            Container(
              width: imageSize,
              height: imageSize,
              decoration: BoxDecoration(
                image: !(networkError[product.thumbnail] ?? false)
                    ? DecorationImage(
                        alignment: Alignment.topCenter,
                        fit: BoxFit.fitWidth,
                        onError: (Object e, StackTrace? stackTrace) {
                          setState(() {
                            networkError[product.thumbnail] = true;
                          });
                        },
                        image: backgroundImage)
                    : DecorationImage(
                        fit: BoxFit.fill,
                        image: backgroundImageFallback,
                        alignment: Alignment.topCenter),
                color: theme!.cardColor,
                borderRadius:
                    const BorderRadius.all(Radius.circular(baseBorderRadius)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 2,
                    offset: const Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: basePadding,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.title,
                    style: theme!.textTheme.titleLarge,
                  ),
                  const SizedBox(
                    height: basePadding / 2,
                  ),
                  Text(
                    product.description,
                    style: theme!.textTheme.bodyMedium,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                  ),
                ],
              ),
            ),
          ],
        ),
      ));
    }

    return tmpProductList;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          context,
          title: Get.arguments["name"],
          backgroundColor: theme!.primaryColor,
          enableLeading: true,
        ),
        body: Container(
          padding: const EdgeInsets.all(basePadding * 2),
          child: Column(
            children: [
              if (products.isNotEmpty) ...{
                Expanded(
                  child: CustomSingleScrollBar(
                    controller: _scrollController,
                    child: Column(
                      children: initResource(),
                    ),
                  ),
                )
              } else ...{
                const Expanded(
                  child: Center(
                    child: CustomLoadingIndicator(),
                  ),
                )
              }
            ],
          ),
        ),
      ),
    );
  }
}
