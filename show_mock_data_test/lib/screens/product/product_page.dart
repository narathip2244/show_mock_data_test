import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:show_mock_data_test/api/product.dart';
import 'package:show_mock_data_test/constants/global_variable.dart';
import 'package:show_mock_data_test/helpers/app_localizations.dart';
import 'package:show_mock_data_test/widgets/custom_app_bar.dart';
import 'package:show_mock_data_test/widgets/custom_loading_indicator.dart';
import 'package:show_mock_data_test/widgets/custom_single_child_scroll_bar.dart';

part 'screens/product_screen.dart';
