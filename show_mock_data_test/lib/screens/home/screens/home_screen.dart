part of '../home_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ScrollController? _scrollController;
  AdvancedDrawerController? _advancedDrawerController;
  List<Categorie> categories = [];
  ThemeData? theme;
  AppLocalizations? i18n;
  Locale? _selectedLocale;
  bool showDrawer = false;
  final Map<String, String> mapOptions = {
    "en": "English",
    "th": "ไทย",
  };

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    i18n = AppLocalizations.of(context);
    theme = Theme.of(context);

    initLocaleName(i18n!.translate('lang'));
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _advancedDrawerController = AdvancedDrawerController();

    fetchCategories().then(
      (value) {
        setState(() {
          categories = value;
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController!.dispose();
    _advancedDrawerController!.dispose();
  }

  initLocaleName(String localeName) {
    switch (localeName) {
      case 'en':
        _selectedLocale = SUPPORTED_LOCALES[0];
      case 'th':
        _selectedLocale = SUPPORTED_LOCALES[1];
      default:
        _selectedLocale = SUPPORTED_LOCALES.first;
    }
  }

  List<Widget> initResource() {
    List<Widget> tmpCategoryList = [];

    for (var category in categories) {
      tmpCategoryList.add(Padding(
        padding: const EdgeInsets.symmetric(vertical: basePadding / 2),
        child: Material(
          color: theme!.cardColor,
          borderRadius: BorderRadius.circular(baseBorderRadius),
          child: InkWell(
            borderRadius: BorderRadius.circular(baseBorderRadius),
            onTap: () {
              Get.toNamed(Routes.product,
                  arguments: {"slug": category.slug, "name": category.name});
            },
            child: Container(
              padding: const EdgeInsets.symmetric(
                  vertical: basePadding * 2, horizontal: basePadding),
              alignment: Alignment.centerLeft,
              child: Text(
                category.name,
                style: theme!.textTheme.titleMedium,
              ),
            ),
          ),
        ),
      ));
    }

    return tmpCategoryList;
  }

  _handleMenuButtonPressed(bool show) {
    if (show) {
      _advancedDrawerController!.showDrawer();
    } else {
      _advancedDrawerController!.hideDrawer();
    }
    setState(() {
      showDrawer = show;
    });
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        if (showDrawer) {
          _handleMenuButtonPressed(false);
        }
      },
      child: AdvancedDrawer(
        backdropColor: theme!.primaryColor,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 250),
        animateChildDecoration: true,
        rtlOpening: false,
        openRatio: 0.7,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(Const.space8)),
        ),
        drawer: SafeArea(
          child: Container(
            padding: const EdgeInsets.all(basePadding * 2),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  i18n!.translate("language"),
                  style: theme!.textTheme.titleLarge,
                ),
                const SizedBox(height: Const.space15),
                const Divider(
                  height: basePadding,
                  thickness: 1,
                  endIndent: 0,
                  color: Color.fromARGB(255, 182, 182, 182),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: SUPPORTED_LOCALES.map((locale) {
                    return Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          AppLocalizations.of(context)
                              .setlang(locale.languageCode);
                          setState(() {
                            _selectedLocale = locale;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(basePadding),
                          child: Row(
                            children: [
                              CountryFlag.fromCountryCode(
                                locale.countryCode ?? "US",
                                width: baseIconSize,
                                height: baseIconSize / 1.5,
                              ),
                              const SizedBox(
                                width: Const.space12,
                              ),
                              Text(
                                mapOptions[locale.languageCode].toString(),
                                style: _selectedLocale == locale
                                    ? theme!.textTheme.titleMedium
                                    : theme!.textTheme.titleMedium,
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ],
            ),
          ),
        ),
        child: SafeArea(
          child: Scaffold(
            appBar: CustomAppBar(
              context,
              title: i18n!.translate('home'),
              backgroundColor: theme!.primaryColor,
              leading: Material(
                color: Colors.transparent,
                child: InkWell(
                    borderRadius: BorderRadius.circular(300),
                    onTap: () {
                      _handleMenuButtonPressed(true);
                    },
                    child: Container(
                      padding: const EdgeInsets.all(basePadding),
                      child: Icon(
                        Icons.menu,
                        color: Colors.black,
                        size: baseIconSize,
                      ),
                    )),
              ),
              enableLeading: false,
              leadingOntap: () {
                _handleMenuButtonPressed(true);
              },
            ),
            body: Container(
              padding: const EdgeInsets.all(basePadding * 2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    i18n!.translate('categories'),
                    style: theme!.textTheme.headlineMedium,
                  ),
                  const SizedBox(height: basePadding),
                  const Divider(
                    height: basePadding,
                    thickness: 1,
                    endIndent: 0,
                    color: Color.fromARGB(255, 182, 182, 182),
                  ),
                  const SizedBox(height: basePadding),
                  if (categories.isNotEmpty) ...{
                    Expanded(
                      child: CustomSingleScrollBar(
                        controller: _scrollController,
                        child: Column(
                          children: initResource(),
                        ),
                      ),
                    )
                  } else ...{
                    const Expanded(
                      child: Center(
                        child: CustomLoadingIndicator(),
                      ),
                    )
                  }
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
