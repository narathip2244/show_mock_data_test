import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: non_constant_identifier_names
AppBar CustomAppBar(
  BuildContext context, {
  String? title,
  bool centerTitle = true,
  bool enableLeading = true,
  Widget? leading,
  List<Widget>? actions,
  void Function()? leadingOntap,
  PreferredSizeWidget? bottom,
  Color? backgroundColor,
}) {
  final theme = Theme.of(context);
  return AppBar(
    backgroundColor:
        (backgroundColor == null) ? theme.primaryColor : backgroundColor,
    centerTitle: centerTitle,
    leading: leading ??
        (enableLeading
            ? IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed:
                    (leadingOntap == null) ? buildBackOnTap : leadingOntap,
                color: Colors.black,
              )
            : const SizedBox()),
    title: Text(title ?? '', style: theme.textTheme.headlineMedium),
    actions: actions,
    bottom: bottom,
  );
}

void buildBackOnTap() {
  Get.back<dynamic>();
}
