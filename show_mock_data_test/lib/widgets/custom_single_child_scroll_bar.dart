import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class CustomSingleScrollBar extends StatelessWidget {
  final Widget child;
  final ScrollController? controller;
  const CustomSingleScrollBar(
      {super.key, required this.child, this.controller});

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: ScrollConfiguration.of(context).copyWith(
        dragDevices: {
          PointerDeviceKind.mouse,
          PointerDeviceKind.touch,
        },
      ),
      child: Scrollbar(
        thickness: 8, //width of scrollbar
        radius: const Radius.circular(20), //corner radius of scrollbar
        scrollbarOrientation:
            ScrollbarOrientation.right, //which side to show scrollbar
        controller: controller,
        child: SingleChildScrollView(controller: controller, child: child),
      ),
    );
  }
}
