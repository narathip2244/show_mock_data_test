/// Size
double baseTextSize = 16; // clamp(16, 18) from main
double baseIconSize = 20; // clamp(20, 30)
const double basePadding = 10; // default
const double baseBorderRadius = 5; // default
