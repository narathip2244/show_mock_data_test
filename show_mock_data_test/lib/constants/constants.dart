import 'package:flutter/material.dart';

class Const {
  static const margin = 18.0;
  static const space25 = 25.0;
  static const space15 = 15.0;
  static const space12 = 12.0;
  static const space8 = 8.0;
}

// ignore: constant_identifier_names
const List<Locale> SUPPORTED_LOCALES = [
  Locale('en', "US"),
  Locale('th', "TH"),
];

class Routes {
  static const String home = '/home';
  static const String language = '/language';
  static const String product = '/product';
}
