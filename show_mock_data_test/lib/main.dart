import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:show_mock_data_test/constants/constants.dart';
import 'package:show_mock_data_test/constants/global_variable.dart';
import 'package:show_mock_data_test/helpers/app_localizations.dart';
import 'package:show_mock_data_test/helpers/route_util.dart';
import 'package:show_mock_data_test/helpers/themes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:show_mock_data_test/providers/providers.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  void setGlobalVariable(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    baseTextSize =
        screenHeight > screenWidth ? screenHeight * 0.01 : screenWidth * 0.01;

    baseIconSize =
        screenHeight > screenWidth ? screenHeight * 0.03 : screenWidth * 0.03;

    // ### set max and min global text sized
    baseTextSize = baseTextSize.clamp(16, 18);
    // ### set max and min global icon sized
    baseIconSize = baseIconSize.clamp(20, 30);
  }

  @override
  Widget build(BuildContext context) {
    setGlobalVariable(context);

    return GetMaterialApp(
      title: 'Show mock data test',
      themeMode: ThemeProvider().isDarkTheme ? ThemeMode.dark : ThemeMode.light,
      darkTheme: themeDark(context),
      theme: themeLight(context),
      locale: const Locale('en'),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: SUPPORTED_LOCALES,
      getPages: allRoutes,
      initialRoute: Routes.home,
      // home:
      // ,
    );
  }
}
