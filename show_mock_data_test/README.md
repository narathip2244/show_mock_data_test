### How to run the app

Run 
    
    make clean
    make run

or

    flutter clean
    flutter run

*** Make sure you update the flutter environment ***

Run 

    make clean

or
     
    flutter clean
    flutter pub get

## How to build app 

### Update App version
- pubspec.yaml    # Change app version

### build Android appbundle for publish

Run 

    make aab 

or
    
    flutter build appbundle

### build Android apk

Run 

    make apk 

or

    flutter build apk

### build Window exe file

Run 

    make win      

or

    flutter build windows


## When change or edit file on Android folder and donen't work
Run 

    make re-gradlew 

or
    
    cd android
    .\gradlew clean or gradlew clean on mac

